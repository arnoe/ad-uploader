/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader.csv;

import java.util.ArrayList;
import java.util.List;

import com.arnoe.util.CSVParser;
import com.arnoe.util.StringUtil;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class AdvertUploadRecordTestcase {
    
    private AdvertUploadRecord instance;
    
    
    @Before
    public void setUp() throws Exception {
        instance = new AdvertUploadRecord(0, createHeaderList());
    }
    
    @Test
    public void testValidate() {
        Assert.assertTrue(instance.validate());
    }
    
    
    
    private List<String> createHeaderList() {
        
        final ArrayList<String> headerList = new ArrayList<String>();
        final CSVParser csvParser = new CSVParser("Status,StatusMessage,AccountSid,AuthToken,Name,CampaignId,AdType,SearchTags,AllowDeepLinking,MobileReady,LandingPage,Language,LimitedTimeStartDate,LimitedTimeEndDate,GetHtmlCodeType,CustomisationCharge,PhoneTracking,PromoCodeTracking,Season,TopSeller,DiscountCategory,DiscountSubCategory,ProductName,CouponCode,DiscountAmount,DiscountPercent,BeforePrice,AfterPrice,MinimumPurchaseAmount,ProductCreativeFile,BannerAlternativeTag,LinkedText,BannerCreativeFile,CustomAdServingUrl,AdCodeTemplate,IabAdUnit,ThirdPartyServableAdCreativeHeight,ThirdPartyServableAdCreativeWidth",
                ',');
        String value;
        while (StringUtil.isNotNullOrEmpty(value = csvParser.nextToken())) {
            headerList.add(value);
        }
        return headerList;
    }
    
    @Test
    public void testAdvertUploadRecord() {
        Assert.fail("Not yet implemented");
    }
    
}
