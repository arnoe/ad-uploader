/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader.csv;

import org.junit.Assert;
import org.junit.Test;


public class AdUploadFieldTestcase {
    
    @Test
    public void testGetApiNames() {
        Assert.assertEquals("Status", AdUploadField.STATUS.getApiName());
        Assert.assertEquals("StatusMessage", AdUploadField.STATUS_MESSAGE.getApiName());
        Assert.assertEquals("AccountSid", AdUploadField.ACCOUNT_SID.getApiName());
        Assert.assertEquals("AuthToken", AdUploadField.AUTH_TOKEN.getApiName());
        Assert.assertEquals("Name", AdUploadField.NAME.getApiName());
        Assert.assertEquals("CampaignId", AdUploadField.CAMPAIGN_ID.getApiName());
        Assert.assertEquals("AdType", AdUploadField.AD_TYPE.getApiName());
        Assert.assertEquals("SearchTags", AdUploadField.SEARCH_TAGS.getApiName());
        Assert.assertEquals("AllowDeepLinking", AdUploadField.ALLOW_DEEP_LINKING.getApiName());
        Assert.assertEquals("MobileReady", AdUploadField.MOBILE_READY.getApiName());
        Assert.assertEquals("LandingPage", AdUploadField.LANDING_PAGE.getApiName());
        Assert.assertEquals("Language", AdUploadField.LANGUAGE.getApiName());
        Assert.assertEquals("LimitedTimeStartDate", AdUploadField.LIMITED_TIME_START_DATE.getApiName());
        Assert.assertEquals("LimitedTimeEndDate", AdUploadField.LIMITED_TIME_END_DATE.getApiName());
        Assert.assertEquals("GetHtmlCodeType", AdUploadField.GET_HTML_CODE_TYPE.getApiName());
        Assert.assertEquals("CustomisationCharge", AdUploadField.CUSTOMISATION_CHARGE.getApiName());
        Assert.assertEquals("PhoneTracking", AdUploadField.PHONE_TRACKING.getApiName());
        Assert.assertEquals("PromoCodeTracking", AdUploadField.PROMO_CODE_TRACKING.getApiName());
        Assert.assertEquals("Season", AdUploadField.SEASON.getApiName());
        Assert.assertEquals("TopSeller", AdUploadField.TOP_SELLER.getApiName());
        Assert.assertEquals("DiscountCategory", AdUploadField.DISCOUNT_CATEGORY.getApiName());
        Assert.assertEquals("DiscountSubCategory", AdUploadField.DISCOUNT_SUB_CATEGORY.getApiName());
        Assert.assertEquals("ProductName", AdUploadField.PRODUCT_NAME.getApiName());
        Assert.assertEquals("CouponCode", AdUploadField.COUPON_CODE.getApiName());
        Assert.assertEquals("DiscountAmount", AdUploadField.DISCOUNT_AMOUNT.getApiName());
        Assert.assertEquals("DiscountPercent", AdUploadField.DISCOUNT_PERCENT.getApiName());
        Assert.assertEquals("BeforePrice", AdUploadField.BEFORE_PRICE.getApiName());
        Assert.assertEquals("AfterPrice", AdUploadField.AFTER_PRICE.getApiName());
        Assert.assertEquals("MinimumPurchaseAmount", AdUploadField.MINIMUM_PURCHASE_AMOUNT.getApiName());
        Assert.assertEquals("ProductCreativeFile", AdUploadField.PRODUCT_CREATIVE_FILE.getApiName());
        Assert.assertEquals("BannerAlternativeTag", AdUploadField.BANNER_ALTERNATIVE_TAG.getApiName());
        Assert.assertEquals("LinkText", AdUploadField.LINK_TEXT.getApiName());
        Assert.assertEquals("BannerCreativeFile", AdUploadField.BANNER_CREATIVE_FILE.getApiName());
        Assert.assertEquals("CustomAdServingUrl", AdUploadField.CUSTOM_AD_SERVING_URL.getApiName());
        Assert.assertEquals("AdCodeTemplate", AdUploadField.AD_CODE_TEMPLATE.getApiName());
        Assert.assertEquals("IabAdUnit", AdUploadField.IAB_AD_UNIT.getApiName());
        Assert.assertEquals("ThirdPartyServableAdCreativeHeight",
                AdUploadField.THIRD_PARTY_SERVABLE_AD_CREATIVE_HEIGHT.getApiName());
        Assert.assertEquals("ThirdPartyServableAdCreativeWidth",
                AdUploadField.THIRD_PARTY_SERVABLE_AD_CREATIVE_WIDTH.getApiName());
    }
}
