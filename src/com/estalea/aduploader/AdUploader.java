/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import com.arnoe.util.EnumUtil;
import com.arnoe.util.StringUtil;
import org.apache.log4j.Logger;

import com.estalea.aduploader.config.AdUploaderPropertyLoader;
import com.estalea.aduploader.config.AdUploaderPropertyLoader.AdUploaderProperty;
import com.estalea.aduploader.csv.AdUploadField;
import com.estalea.aduploader.csv.AdvertUploadCSV;
import com.estalea.aduploader.csv.AdvertUploadRecord;
import com.estalea.aduploader.csv.AdvertUploadRecord.Status;
import com.estalea.aduploader.rest.RestClient;

/**
 * @author arnoe
 */
public class AdUploader {
    
    private static Logger log = Logger.getLogger(AdUploader.class);
    private static AdUploaderPropertyLoader propertyLoader;


    static {
        try {
            propertyLoader = new AdUploaderPropertyLoader();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



    private void uploadBulkAdverts(String csvFilePath) throws IOException {
        // Load CSV
        final AdvertUploadCSV adUploadCsv = new AdvertUploadCSV(csvFilePath);
        log.info("Loaded file " + adUploadCsv.getAbsolutePath());
        
        // Read AdvertRecords from CSV
        final List<AdvertUploadRecord> currentCVSRecords = adUploadCsv.readRecordsWithoutHeader();
        final ArrayList<AdvertUploadRecord> newCVSRecords = new ArrayList<AdvertUploadRecord>();
        
        final RestClient restClient = createRestClient();
        
        int uploadedCount = 0;
        int failedCount = 0;
        int skippedCount = 0;
        for (final AdvertUploadRecord record : currentCVSRecords) {
            String currentStatusStr = record.getValue(AdUploadField.STATUS);
            if (StringUtil.isNullOrEmpty(currentStatusStr)) {
                currentStatusStr = Status.NEW.name();
            }
            
            final Status currentStatus = EnumUtil.toEnum(Status.class, currentStatusStr);
            
            if (currentStatus == Status.NEW || currentStatus == Status.FAILED) {
                try {
                    // Send Message to Rest Service
                    final String message = restClient.executeMultiPartRequest(record).replace("\n", "")
                            .replace(";", "\\;").replace("\t", "  ").replace("  ", " ");
                    if (message.contains("<Status>OK</Status>")) {
                        record.setValue(AdUploadField.STATUS, Status.SUCCESS.name());
                        uploadedCount++;
                    } else {
                        record.setValue(AdUploadField.STATUS, Status.FAILED.name());
                        failedCount++;
                    }
                    record.setValue(AdUploadField.STATUS_MESSAGE, message);
                    
                } catch (final UnsupportedEncodingException e) {
                    record.setValue(AdUploadField.STATUS_MESSAGE, e.getMessage());
                    record.setValue(AdUploadField.STATUS, Status.FAILED.name());
                    log.error(e);
                    failedCount++;
                } catch (final FileNotFoundException e) {
                    record.setValue(AdUploadField.STATUS_MESSAGE, e.getMessage());
                    record.setValue(AdUploadField.STATUS, Status.FAILED.name());
                    log.error(e);
                    failedCount++;
                } catch (final Exception e) {
                    record.setValue(AdUploadField.STATUS_MESSAGE, e.getMessage());
                    record.setValue(AdUploadField.STATUS, Status.FAILED.name());
                    log.error(e);
                    failedCount++;
                }
                log.debug(String.format("Details: %s", record.toString()));
            } else {
                skippedCount++;
            }
            newCVSRecords.add(record);
        }
        
        adUploadCsv.writeRecords(newCVSRecords);
        log.info("total processed: " + adUploadCsv.readRecordsWithoutHeader().size());
        log.info("skipped records: " + skippedCount);
        if (uploadedCount == 0 && failedCount == 0) {
            log.info("NO RECORDS PROCESSED.");
        } else {
            log.info("passed records: " + uploadedCount);
            log.info("failed records: " + failedCount);
        }
    }
    
    private RestClient createRestClient() throws FileNotFoundException, IOException {
        log.info("Loading Configuration...");


        // Load Configuration
        final String server = propertyLoader.getProperty(AdUploaderProperty.BASE_URI);
        final String resourceName = propertyLoader.getProperty(AdUploaderProperty.RESOURCE_NAME);
        
        return new RestClient(server, resourceName);
    }

    public enum Argument {
        UPLOAD_ADS,DOWNLOAD_EMPTY_CSV_SAMPLE
    }
    
    public static void main(String[] args)  {

        try {
            if(args.length == 2)
            {
                String option = args[0];
                if(StringUtil.isNotNullOrEmpty(option)){
                    if(Argument.valueOf(option) == Argument.UPLOAD_ADS){
                        String csvFile = args[1];
                        log.info(String.format("Processing %s : %s", Argument.UPLOAD_ADS.name(), csvFile));
                            new AdUploader().uploadBulkAdverts(csvFile);
                        log.info("Processing complete");
                    }else if (Argument.valueOf(option) == Argument.DOWNLOAD_EMPTY_CSV_SAMPLE){
                        log.info(String.format("Processing %s : %s", Argument.UPLOAD_ADS.name(), option));
                        new AdUploader().uploadBulkAdverts(option);
                        log.info("Processing complete");
                    }else{
                        printHelpText();
                    }
                }
            }else{
                 printHelpText();
            }
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
//
//        try {
//            if (args.length == 1) {
//                new AdUploader().uploadBulkAdverts(args[0]);
//                log.info("Processing complete");
//            } else {
//                log.error("Usage: java -jar [PATH TO JAR FILENAME] [PATH TO CSV FILENAME]");
//            }
//        } catch (final IOException e) {
//            log.error(e);
//        }
    }

    private static void printHelpText() {
        StringBuilder builder = new StringBuilder("\n\n\n\n                      Bulk Ad Uploader\n\n");
        builder.append("                  General Commands Manual    \n");
        builder.append("\n");
        builder.append("version %s\n");
        builder.append("\n");
        builder.append("SYNOPSIS\n");
        builder.append("          java [ options ] -jar file.jar [ argument ... ]\n");
        builder.append("\n");
        builder.append("          options\n");
        builder.append("             Command-line options of java.\n");
        builder.append("\n");
        builder.append("          file.jar\n");
        builder.append("             Name of the jar file to be invoked. Used only with -jar.\n");
        builder.append("\n");
        builder.append("          argument\n");
        builder.append("             Argument passed to the main function.\n");
//        builder.append(String.format("               - %s \n", Argument.DOWNLOAD_EMPTY_CSV_SAMPLE.name()));
        builder.append(String.format("               - %s [ file.csv ]\n", Argument.UPLOAD_ADS.name()));
        builder.append("\n");
        builder.append("          options\n");
        builder.append("             Command-line options of java.\n");
        builder.append("\n");
        builder.append("          file.jar\n");
        builder.append("             Name of the jar file to be invoked. Used only with -jar.\n");
        builder.append("\n");
        builder.append("");
        builder.append("");
        String version = propertyLoader.getProperty(AdUploaderProperty.VERSION);
        String value = String.format(builder.toString(), version, Argument.DOWNLOAD_EMPTY_CSV_SAMPLE.name(), Argument.UPLOAD_ADS.name());
        System.out.println(value);
    }
}
