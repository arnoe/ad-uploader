/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader.config;

import java.io.FileNotFoundException;
import java.io.IOException;

import com.arnoe.util.PropertyLoader;
import com.estalea.aduploader.config.AdUploaderPropertyLoader.AdUploaderProperty;



public class AdUploaderPropertyLoader extends PropertyLoader<AdUploaderProperty> {

    public AdUploaderPropertyLoader() throws FileNotFoundException, IOException {
        super(AdUploaderProperty.class, "config.properties");
    }
    
    public enum AdUploaderProperty {
        BASE_URI, VERSION, RESOURCE_NAME
    }
    
}
