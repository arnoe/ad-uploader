/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader.csv;

import java.util.List;
import java.util.Set;

import com.arnoe.util.StringUtil;
import com.arnoe.util.csv.CsvRecord;

public class AdvertUploadRecord extends CsvRecord<AdUploadField> {
    
    private final int record;
    
    public AdvertUploadRecord(int record, List<String> values) {
        super(AdUploadField.class, values);
        this.record = record;
    }
    
    public enum Status {
        NEW, SUCCESS, FAILED
    }
    
    @Override
    public boolean validate() {
        if (!super.validate()) {
            return false;
        }
        
        if (record == 0) { // Validate Header
            final Set<AdUploadField> headerFields = header_value.keySet();
            for (final AdUploadField adUploadField : headerFields) {
                final String headerValue = header_value.get(adUploadField);
                if (headerValue != adUploadField.getApiName()) {
                    
                    System.err.println(String.format("Column header did not match : got '%s', but expected '%s'",
                            headerValue,
                            adUploadField.getApiName()));
                    return false;
                }
            }
        }
        
        return true;
    }
    
    @Override
    public String toString() {
        if (!StringUtil.isNullOrEmpty(getValue(AdUploadField.BANNER_CREATIVE_FILE))) {
            return String.format("Upload(status=%s,bannerCreative=%s)",
                    getValue(AdUploadField.STATUS),
                    getValue(AdUploadField.BANNER_CREATIVE_FILE));
        }
        if (!StringUtil.isNullOrEmpty(getValue(AdUploadField.PRODUCT_CREATIVE_FILE))) {
            return String.format("Upload(status=%s,productCreative=%s)",
                    getValue(AdUploadField.STATUS),
                    getValue(AdUploadField.PRODUCT_CREATIVE_FILE));
        }
        return String.format("Upload(status=%s)", getValue(AdUploadField.STATUS));
    }
}
