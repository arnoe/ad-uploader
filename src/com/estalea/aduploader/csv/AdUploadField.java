/*
 * Copyright 2013 Estalea("8888"), Inc. All rights reserved.
 * This software is the proprietary information of Estalea("8888"), Inc.
 */
package com.estalea.aduploader.csv;


public enum AdUploadField {
    
    STATUS("Status"),
    STATUS_MESSAGE("StatusMessage"),
    ACCOUNT_SID("AccountSid"),
    AUTH_TOKEN("AuthToken"),
    NAME("Name"),
    CAMPAIGN_ID("CampaignId"),
    AD_TYPE("AdType"),
    SEARCH_TAGS("SearchTags"),
    ALLOW_DEEP_LINKING("AllowDeepLinking"),
    MOBILE_READY("MobileReady"),
    LANDING_PAGE("LandingPage"),
    LANGUAGE("Language"),
    LIMITED_TIME_START_DATE("LimitedTimeStartDate"),
    LIMITED_TIME_END_DATE("LimitedTimeEndDate"),
    GET_HTML_CODE_TYPE("GetHtmlCodeType"),
    CUSTOMISATION_CHARGE("CustomisationCharge"),
    PHONE_TRACKING("PhoneTracking"),
    PROMO_CODE_TRACKING("PromoCodeTracking"),
    SEASON("Season"),
    TOP_SELLER("TopSeller"),
    DISCOUNT_CATEGORY("DiscountCategory"),
    DISCOUNT_SUB_CATEGORY("DiscountSubCategory"),
    PRODUCT_NAME("ProductName"),
    COUPON_CODE("CouponCode"),
    DISCOUNT_AMOUNT("DiscountAmount"),
    DISCOUNT_PERCENT("DiscountPercent"),
    BEFORE_PRICE("BeforePrice"),
    AFTER_PRICE("AfterPrice"),
    MINIMUM_PURCHASE_AMOUNT("MinimumPurchaseAmount"),
    PRODUCT_CREATIVE_FILE("ProductCreativeFile"),
    BANNER_ALTERNATIVE_TAG("BannerAlternativeTag"),
    LINK_TEXT("LinkText"),
    BANNER_CREATIVE_FILE("BannerCreativeFile"),
    CUSTOM_AD_SERVING_URL("CustomAdServingUrl"),
    AD_CODE_TEMPLATE("AdCodeTemplate"),
    IAB_AD_UNIT("IabAdUnit"),
    THIRD_PARTY_SERVABLE_AD_CREATIVE_HEIGHT("ThirdPartyServableAdCreativeHeight"),
    THIRD_PARTY_SERVABLE_AD_CREATIVE_WIDTH("ThirdPartyServableAdCreativeWidth");
    
    private String apiName;
    
    AdUploadField() {
    }
    
    AdUploadField(String apiName) {
        this.apiName = apiName;
    }
    
    /**
     * @return
     */
    public String getApiName() {
        return apiName;
    }
}
