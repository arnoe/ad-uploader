/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader.csv;

import java.io.FileNotFoundException;
import java.util.List;

import com.arnoe.util.csv.CsvFile;


@SuppressWarnings("serial")
public class AdvertUploadCSV extends CsvFile<AdvertUploadRecord> {

    public AdvertUploadCSV(String filePath) throws FileNotFoundException {
        super(filePath, true, ';');
    }

    @Override
    protected AdvertUploadRecord createRecord(int recordCount, List<String> lineStr) {
        return new AdvertUploadRecord(recordCount, lineStr);
    }
}
