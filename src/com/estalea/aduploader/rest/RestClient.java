/*
 * Copyright 2013 Estalea, Inc. All rights reserved.
 * This software is the proprietary information of Estalea, Inc.
 */
package com.estalea.aduploader.rest;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;

import com.estalea.aduploader.csv.AdUploadField;
import com.estalea.aduploader.csv.AdvertUploadRecord;


public class RestClient {
    
    private final String baseUri;
    private final String resourceName;
    
    private static Logger log = Logger.getLogger(RestClient.class);
    
    public RestClient(String baseUri, String resourceName) {
        this.baseUri = baseUri;
        this.resourceName = resourceName;
    }
    
    /**
     * Method that builds the multi-part form data request
     * 
     * @param record
     * @return
     * @throws IOException
     * @throws ClientProtocolException
     */
    public String executeMultiPartRequest(AdvertUploadRecord record) throws ClientProtocolException, IOException {
        
        
        final String accountSid = record.getValue(AdUploadField.ACCOUNT_SID);
        final StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(String.format("%s/%s/%s", baseUri, accountSid, resourceName));
        
        log.debug("Chatting to : " + stringBuilder.toString());
        final HttpPost postRequest = new HttpPost(stringBuilder.toString());
        final MultipartEntity multiPartEntity = new MultipartEntity();
        //The usual form parameters can be added this way
        
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.NAME);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.CAMPAIGN_ID);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.AD_TYPE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.SEARCH_TAGS);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.ALLOW_DEEP_LINKING);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.MOBILE_READY);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.LANDING_PAGE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.LANGUAGE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.LIMITED_TIME_START_DATE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.LIMITED_TIME_END_DATE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.GET_HTML_CODE_TYPE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.CUSTOMISATION_CHARGE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.PHONE_TRACKING);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.PROMO_CODE_TRACKING);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.SEASON);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.TOP_SELLER);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.DISCOUNT_CATEGORY);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.DISCOUNT_SUB_CATEGORY);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.PRODUCT_NAME);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.COUPON_CODE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.DISCOUNT_AMOUNT);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.DISCOUNT_PERCENT);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.BEFORE_PRICE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.AFTER_PRICE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.MINIMUM_PURCHASE_AMOUNT);
        
        // Product Creative
        final File productCreative = new File(record.getValue(AdUploadField.PRODUCT_CREATIVE_FILE));
        if (productCreative.exists()) {
            final FileBody fileBody = new FileBody(productCreative, "image/jpeg");
            multiPartEntity.addPart(AdUploadField.PRODUCT_CREATIVE_FILE.getApiName(), fileBody);
        }
        
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.BANNER_ALTERNATIVE_TAG);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.LINK_TEXT);
        
        // Banner Creative
        final File bannerCreative = new File(record.getValue(AdUploadField.BANNER_CREATIVE_FILE));
        if (bannerCreative.exists()) {
            final FileBody fileBody = new FileBody(bannerCreative, "image/jpeg");
            multiPartEntity.addPart(AdUploadField.BANNER_CREATIVE_FILE.getApiName(), fileBody);
        }
        
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.CUSTOM_AD_SERVING_URL);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.AD_CODE_TEMPLATE);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.IAB_AD_UNIT);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.THIRD_PARTY_SERVABLE_AD_CREATIVE_HEIGHT);
        addToMultiPartEntity(multiPartEntity, record, AdUploadField.THIRD_PARTY_SERVABLE_AD_CREATIVE_WIDTH);
        
        // Basic Authentication
        final String authToken = record.getValue(AdUploadField.AUTH_TOKEN);
        final UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(accountSid, authToken);
        postRequest.setHeader(BasicScheme.authenticate(credentials, "UTF-8", false));
        
        postRequest.setEntity(multiPartEntity);
        
        return executeRequest(postRequest, accountSid, authToken);
    }
    
    private void addToMultiPartEntity(final MultipartEntity multiPartEntity, AdvertUploadRecord record,
            AdUploadField field) throws UnsupportedEncodingException {
        multiPartEntity.addPart(field.getApiName(), new StringBody(record.getValue(field)));
    }
    
    
    //-----------------------------------------------
    // Generic Helpers
    
    
    private static String executeRequest(HttpRequestBase requestBase, String accountSid, String authToken)
            throws ClientProtocolException, IOException {
        String responseString = "";
        
        InputStream responseStream = null;
        final HttpClient client = new DefaultHttpClient();
        try {
            final HttpResponse response = client.execute(requestBase);
            
            if (response != null) {
                final HttpEntity responseEntity = response.getEntity();
                
                if (responseEntity != null) {
                    responseStream = responseEntity.getContent();
                    if (responseStream != null) {
                        final BufferedReader br = new BufferedReader(new InputStreamReader(responseStream));
                        String responseLine = br.readLine();
                        String tempResponseString = "";
                        while (responseLine != null) {
                            tempResponseString = tempResponseString + responseLine
                                    + System.getProperty("line.separator");
                            responseLine = br.readLine();
                        }
                        br.close();
                        if (tempResponseString.length() > 0) {
                            responseString = tempResponseString;
                        }
                    }
                }
            }
        } finally {
            if (responseStream != null) {
                try {
                    responseStream.close();
                } catch (final IOException e) {
                    e.printStackTrace();
                }
            }
        }
        client.getConnectionManager().shutdown();
        
        return responseString;
    }
}
